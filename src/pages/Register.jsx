import styled from "styled-components";
import { mobile } from "../responsive";
import { useDispatch  } from "react-redux";
import { register } from "../redux/apiCalls";
import { useState } from "react";
import { useHistory } from 'react-router-dom';

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: linear-gradient(
      rgba(255, 255, 255, 0.5),
      rgba(255, 255, 255, 0.5)
    ),
    url("https://images.pexels.com/photos/6984661/pexels-photo-6984661.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940")
      center;
  background-size: cover;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Wrapper = styled.div`
  width: 40%;
  padding: 20px;
  background-color: white;
  ${mobile({ width: "75%" })}
`;

const Title = styled.h1`
  font-size: 24px;
  font-weight: 300;
`;

const Form = styled.form`
  display: flex;
  flex-wrap: wrap;
`;

const Input = styled.input`
  flex: 1;
  min-width: 40%;
  margin: 20px 10px 0px 0px;
  padding: 10px;
`;

const Agreement = styled.span`
  font-size: 12px;
  margin: 20px 0px;
`;

const Button = styled.button`
  width: 40%;
  border: none;
  padding: 15px 20px;
  background-color: teal;
  color: white;
  cursor: pointer;
`;

const Register = () => {
  const [name,setName] = useState("");
  const [lastname,setLastame] = useState("");
  const [username, setUsername] = useState("");  
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const history = useHistory();
  
const handleClick = (e) => {
  e.preventDefault();
  register(dispatch, { name,lastname,email, username, password });
  history.push('/login');

};
  return (
    <Container>
      <Wrapper>
        <Title>CREATE AN ACCOUNT</Title>
        <Form>
          <Input type="text" placeholder="name" onChange={(e) => setName(e.target.value) }/>
          <Input type="text" placeholder="last name" onChange={(e) => setLastame(e.target.value)} />
          <Input type="text" placeholder="username" onChange={(e) => setUsername(e.target.value)} />
          <Input type="email"placeholder="email" onChange={(e) => setEmail(e.target.value) }/>
          <Input type="password" placeholder="password" onChange={(e) => setPassword(e.target.value)} />
          <Agreement>
          
          </Agreement>
          <Button onClick={handleClick}>CREATE</Button>
        </Form>
      </Wrapper>
    </Container>
  );
};

export default Register;
