import { combineReducers } from 'redux';
import userReducer from './userSlice';
import cartReducer from './cartSlice';

const rootReducer = combineReducers({
  user: userReducer,
  carts: (state = {}, action) => {
    const userId = action.payload ? action.payload.userId : null;
    if (userId && state[userId]) {
      return {
        ...state,
        [userId]: cartReducer(state[userId], action),
      };
    }
    return state;
  },
});

export default rootReducer;